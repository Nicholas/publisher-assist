package Publication;

import Author.Author;
import LiveSystem.LiveView;
import PublicationExternal.PublicationView;
import PublicationExternal.PublicationViewDecorator;
import PublicationInternal.Draft;
import PublicationInternal.Live;
import PublicationInternal.Notifiaction;
import PublicationInternal.Pending;
import PublicationInternal.PublicationState;
import Report.Report;

/**
 * A connection between authors and reports
 * 
 * @author Nicholas Welters
 *
 */
public class Publication
{
	private Author [] authors;
	
	private Report report;

	private PublicationState state;
	
	private PublicationState state_Draft;
	private PublicationState state_Pending;
	private PublicationState state_Live;
	private PublicationState state_Notification;;
	
	public Publication( Author [] authors, Report report )
	{
		this.authors = authors;
		this.report = report;
	}
	
	public Author [] getAuthors( )
	{
		return authors;
	}
	
	public Report getReport( )
	{
		return report;
	}
	
	public void setDraft( String parent )
	{
		if( state_Draft == null )
		{
			state_Draft = new Draft( this, parent );
		}
		
		state = state_Draft;
		
		for( int i = 0; i < authors.length; i++ )
		{
			authors[ i ].addDraft( report.getHeading( ) );
		}
	}
	
	public void setNotifiction( PublicationViewDecorator notification )
	{
		if( state_Notification == null )
		{
			state_Notification = new Notifiaction( this );
		}
		
		state = state_Notification;
		
		for( int i = 0; i < authors.length; i++ )
		{
			authors[ i ].remove( report.getHeading( ) );
			
			authors[ i ].addNotification( notification );
		}
	}

	public void setLive( PublicationView thisLive )
	{
		if( state_Live == null )
		{
			state_Live = new Live( this, thisLive );
		}
		
		state = state_Live;
		
		for( int i = 0; i < authors.length; i++ )
		{
			
			authors[ i ].setLive( report.getHeading( ) );
		}
	}

	public void setPending( )
	{
		if( state_Pending == null )
		{
			state_Pending = new Pending( );
		}
		
		state = state_Pending;
		
		for( int i = 0; i < authors.length; i++ )
		{
			authors[ i ].setPending( report.getHeading( ) );
		}
	}

	public void rejected( )
	{
		state = state_Draft;
		
		for( int i = 0; i < authors.length; i++ )
		{
			authors[ i ].setDraft( report.getHeading( ) );
		}
	}

	public String[] getFunctions( )
	{
		return state.getFunctions( );
	}

	public boolean delete( )
	{
		return state.delete( );
	}

	public boolean publish( String[] checks, LiveView liveView )
	{
		return state.publish( checks, liveView );
	}

	public boolean edit( String text )
	{
		return state.edit( text );
	}

	public String comment( String[] authorNames )
	{
		return state.comment( report.getHeading( ), authorNames );
	}
}
