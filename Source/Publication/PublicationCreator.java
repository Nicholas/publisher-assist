package Publication;

import Tools.CreatorStratergy;

public class PublicationCreator implements CreatorStratergy< Publication, PublicationKey >
{
	@ Override
	public Publication FactoryMethod( PublicationKey key )
	{
		return new Publication( key.getAuthors( ), key.getReport( ) );
	}
}
