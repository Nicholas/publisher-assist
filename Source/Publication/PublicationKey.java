package Publication;

import Author.Author;
import Report.Report;
import Tools.FlyweightKey;

public class PublicationKey extends FlyweightKey< Publication >
{
	private Author[] authors;
	private Report report;
	
	public PublicationKey( Author[] authors, Report report )
	{
		super( report.getHeading( ) );
		this.setAuthors( authors );
		this.setReport( report );
	}

	private void setReport( Report report )
	{
		this.report = report;
	}

	public Report getReport( )
	{
		return report;
	}

	private void setAuthors( Author[] authors )
	{
		this.authors = authors;
	}

	public Author[] getAuthors( )
	{
		return authors;
	}

}
