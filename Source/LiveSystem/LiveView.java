package LiveSystem;

import java.util.LinkedList;

import PublicationExternal.PublicationView;
import PublicationExternal.PublicationViewComposite;

public class LiveView
{
	private static final String ROOT = "";
	
	private PublicationView root;
	private LinkedList< PublicationView > liveView;
	
	public LiveView( )
	{
		root = new PublicationViewComposite( ROOT );
		liveView = new LinkedList< PublicationView >( );
	}
	
	public boolean add( String parent, PublicationView child )
	{
		if( parent.equals( ROOT ) )
		{
			root.add( child );
			
			liveView.add( child );
			
			return true;
		}
		else
		{
			PublicationView node = find( parent, root );
			
			if( node != null )
			{
				node.add( child );
				
				return true;
			}
		}
		
		return false;
	}
	
	private PublicationView find( String search, PublicationView node )
	{
		PublicationView view = null;
		
		if( search.equals( node.getPublication( ) ) )
		{
			view = node;
		}
		else
		{
			for( int i = 0; i < node.size( ); i++ )
			{
				view = find( search, node.getChild( i ) );
				
				if( view != null )
				{
					return view;
				}
			}
		}
		
		return view;
	}

	public LinkedList< PublicationView > getLiveView( )
	{
		return liveView;
	}
}
