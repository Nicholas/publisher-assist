package Tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class PrivateClassLoader extends ClassLoader
{
	/**
	 * Single instance created upon class loading.
	 */
	private static final PrivateClassLoader INSTANCE =  new PrivateClassLoader( );


	/**
	 * Get the only instance we ever want to create for the class loader so that all classes loaded are part of the same hierarchical structure
	 */
	public static PrivateClassLoader getInstance() 
	{
		return INSTANCE;
	}

	/**
	 * Private constructor prevents construction.
	 */
	private PrivateClassLoader( )
    {
        super( PrivateClassLoader.class.getClassLoader( ) );
    }

    public Class< ? > loadClass( String name ) throws ClassNotFoundException
    {
        Class< ? > theClass = null;
        
        try
        {
            theClass = super.loadClass( name );
        }
        catch( ClassNotFoundException e )
        {
            
        }
        
        if( theClass != null )
        {
            return theClass;
        }

        char pathSep = '/';
        String fileStub = name.replace( '.', pathSep );
  
        //Build java and class file names from the stub.
        String cf = fileStub + ".class";

        try 
        {
            byte[] classData = getbytes( cf );

            theClass = defineClass( name, classData, 0, classData.length );

        }
        catch( Exception e )
        {
            System.out.println( "defineClass call " + theClass + "( " + name + " ) FAILED" );
        }

        return theClass;
    }
    
    //Give the file on the disk and this method returns the byte array.
    private byte[] getbytes( String filename ) throws IOException
    {
        //Creates file object with given file name. If file not
        //exists throw FileNotFoundException else get the length of file.
        File file = new File( filename );
        
        if( ! file.exists( ) )
        {
            throw new FileNotFoundException( );
        }
        
        long fileLength = file.length();
  
        //Preparing byte array.
        byte[] data = new byte[ ( int ) fileLength ];
  
        //Open the file and get the file stream.
        FileInputStream fis = new FileInputStream( file );
        
        int r = fis.read( data );
        
        fis.close( );
  
        //Checks if all the data is read returns byte array or just
        //returns null.
        if( r != fileLength )
        {
            return null;
        }
        
        return data;
    }
}
