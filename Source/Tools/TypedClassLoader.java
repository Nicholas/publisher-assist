package Tools;

import java.io.File;
import java.util.ArrayList;

public class TypedClassLoader< Type >
{
    static private String JAVA_EXT = ".java";
    static private String CLASS_EXT = ".class";
    
    private String[] avalableClasses;
    private String path = "";
    
    /**
     * 
     * @param directory - Where the .class or .java files can be found - This is then prefixed to the class name for its package
     * It should accept paths with \\ or / separators.
     */
    public TypedClassLoader( String directory )
    {
        //classLoader = new GFClassLoader( );
        //classLoader = GFClassLoader.getInstance( );
        
        path = directory;
        path = path.replace('/', '.').replace('\\', '.') + ".";
        
        if( path.endsWith( ".." ) )
        {
            path = path.substring( 0, path.length( ) - 1 );
        }
        
        File file  = new File( directory );
        
        String[] contents = new String[ 0 ];
        
        if( file.isDirectory( ) )
        {
            contents = file.list( );
        }

        ArrayList< String > javaFiles = new ArrayList< String >( );
        ArrayList< String > classFiles = new ArrayList< String >( );
        
        for( int i = 0; i < contents.length; ++i )
        {
        	if( ! contents[ i ].contains("$") )
        	{
	            if( contents[ i ].endsWith( JAVA_EXT ) )
	            {
	                javaFiles.add( contents[ i ].substring( 0, contents[ i ].length( ) - JAVA_EXT.length( ) ) );
	            }
	            else if ( contents[ i ].endsWith( CLASS_EXT ) )
	            {
	                classFiles.add( contents[ i ].substring( 0, contents[ i ].length( ) - CLASS_EXT.length( ) ) );
	            }
        	}
        }
        

        ArrayList< String > classes = new ArrayList< String >( javaFiles );
        
        boolean found;
        for( int i = 0; i < classFiles.size( ); ++i )
        {
            found = false;
            
            for( int j = 0; j < javaFiles.size( ); j++ )
            {
                if( javaFiles.get( j ).equals( classFiles.get( i ) ) )
                {
                    found = true;
                    j = classFiles.size( );
                }
            }
            
            if( ! found )
            {
                classes.add( classFiles.get( i ) );
            }
        }
        
        avalableClasses = new String[ classes.size( ) ];
        
        for( int i = 0; i < avalableClasses.length; ++i )
        {
            avalableClasses[ i ] = classes.get( i );
        }
    }
    
    
    public String[] getAvalableObjects( )
    {
        return avalableClasses;
    }
    
    @ SuppressWarnings( "unchecked" )
    public Type getClass( String name ) throws ClassNotFoundException
    {
        //Class< ? > theClass = classLoader.loadClass( path + name, true );
        Class< ? > theClass = PrivateClassLoader.getInstance( ).loadClass( path + name );

        Type newObject = null;
        
        try
        {
            newObject = ( Type ) theClass.newInstance( );
        }
        catch( InstantiationException e )
        {
            e.printStackTrace();
        }
        catch( IllegalAccessException e )
        {
            e.printStackTrace();
        }

        return newObject;
    }
}