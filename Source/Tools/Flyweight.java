package Tools;

import java.util.HashMap;
import java.util.Map;

public class Flyweight< Type, Key extends FlyweightKey< Type > >
{
	//////////////
	// Members //
	////////////
	private Map< String, Type > map;
	
	private CreatorStratergy< Type, Key > creator;
	
	public Flyweight( Type defaultData, CreatorStratergy< Type, Key > creator )
	{
		map = new HashMap< String, Type >( );
		this.creator = creator;
		
		map.put( "Default", defaultData );
	}
	
	public Type get( Key key )
	{
		if( ! map.containsKey( key.getKey( ) ) )
		{
			Type obj = creator.FactoryMethod( key );
			
			if( obj == null )
			{
				key.setKey( "Default" );
			}
			else
			{
				map.put( key.getKey( ), obj );
			}
		}
		
		return map.get( key.getKey( )  );
	}

	public boolean containsKey( Key key )
	{
		return map.containsKey( key.getKey( ) );
	}

	public Type remove( Key key )
	{
		// TODO Auto-generated method stub

		return map.remove( key.getKey( ) );
	}
}
