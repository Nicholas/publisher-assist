package Tools;

public class FlyweightKey< Type >
{
	private String key;

	public FlyweightKey( String key )
	{
		this.setKey( key );
	}

	public void setKey( String key )
	{
		this.key = key;
	}

	public String getKey( )
	{
		return key;
	}
}
