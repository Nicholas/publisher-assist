package Tools;

public interface CreatorStratergy< Type, Key extends FlyweightKey< Type > >
{
	public Type FactoryMethod( Key key );
}
