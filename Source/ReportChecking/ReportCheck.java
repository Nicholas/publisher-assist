package ReportChecking;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Publication.Publication;

/**
 * This is a super class for the template method for report checking
 * 
 * @author Nicholas Welters
 *
 */
abstract public class ReportCheck implements Runnable
{
	private Queue< ResultListener > results;

    // Thread Management (Single executor thread)
    private ExecutorService executorService;
	
	public ReportCheck( )// Give the accept and the reject
	{
		this.results = new LinkedList< ResultListener >( );
		
		this.executorService = Executors.newSingleThreadExecutor( );
	}
	
	public void add( ResultListener result )
	{
		if( results.offer( result ) )
		{
			executorService.execute( this );
		}
	}
	
	private ResultListener activeResult = null;
	
	public void run( )
	{
		try
		{
			activeResult = results.remove( );

			checkReport( activeResult.getPublication( ) );
		}
		catch( NoSuchElementException e )
		{
			e.printStackTrace( );
			
			return;
		}
	}
	
	protected void accept( )
	{
	    activeResult.accept( );
	}
	
	protected void reject( )
	{
	    activeResult.reject( getErrorType( ) );
	}
	
	public abstract void checkReport( Publication publication );
	public abstract String getErrorType( );
}
