package ReportChecking;


public interface RejectCommand
{
    public void execute( String errorMessage );
}
