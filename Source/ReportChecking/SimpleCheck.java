package ReportChecking;

import Publication.Publication;

public class SimpleCheck extends ReportCheck
{
	@ Override
	public void checkReport( Publication publication )
	{
		accept( );
	}

	@ Override
	public String getErrorType( )
	{
		return "Very simple default class which will never show this message ever ever!";
	}
}
