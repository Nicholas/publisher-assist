package ReportChecking;

import java.util.Iterator;
import java.util.LinkedList;

import Publication.Publication;

/**
 * This builder creates a testing chain from a list of available test algorithms
 * 
 * @author Nicholas Welters
 *
 */

public class AcceptanceTestBuilder
{
    private static final AcceptanceTestBuilder INSTANCE = new AcceptanceTestBuilder( );
    
    // Current build information
    private Publication publication;
    private LinkedList< ReportCheck > reportChecks;
    
    public static AcceptanceTestBuilder getInstance( )
    {
        return INSTANCE;
    }
    
    private AcceptanceTestBuilder( )
    {
        publication = null;
        reportChecks = new LinkedList< ReportCheck >( );
    }
    
    // Simple interface for running the whole thing in one call;
    public void buildAcceptanceTest( Publication publication, String[] checks, AcceptCommand accept, RejectCommand reject )
    {
        buildAcceptanceTest( publication );
        
        for( int i = 0; i < checks.length; i++ )
        {
            addTest( checks[ i ] );
        }
        
        runTests( accept, reject );
    }
    
    public void buildAcceptanceTest( Publication publication )
    {
        // Do stuff only if we are not already in build mode;
        if( this.publication == null )
        {
            this.publication = publication;
        }
    }
    
    public String[] getAcceptanceTests( )
    {
        return ReportCheckFlyweight.getInstance( ).getAvalableChecks( );
    }
    
    public void addTest( String name )
    {
        // don't do anything if we are not in the build mode.
        if( publication != null )
        {
            reportChecks.add( ReportCheckFlyweight.getInstance( ).getCheck( name ) );
        }
    }
    
    // Completes the test construction and runs them.
    public void runTests( AcceptCommand accept, RejectCommand reject )
    {
        if( publication != null )
        {
            AcceptCommand firstAccept = accept;
            
            if( reportChecks.size( ) > 0 )
            {
                Iterator< ReportCheck > checks = reportChecks.iterator( );
                
                while( checks.hasNext( ) )
                {
                    ResultListener link = new ResultListener( publication, firstAccept, reject );
                    
                    ReportCheck check = checks.next( );
                    
                    firstAccept = new CheckLink( check, link );
                }
            }

            firstAccept.execute( );
            
            // Current build cleanup
            publication = null;
            
            if( ! reportChecks.isEmpty( ) )
            {
                reportChecks.clear( );
            }
        }
    }
}
