package ReportChecking;

import Publication.Publication;
import PublicationExternal.PublicationViewDecorator;
import PublicationExternal.PublicationViewLeaf;
import Server.Server;

public class Reject implements RejectCommand
{
    private Publication publication;
    public Reject( Publication publication )
    {
        this.publication = publication;
    }
    
    @ Override
    public void execute( String errorMessage )
    {
        // TODO
        // NOTIFY THAT THERE WAS AN ERROR!;
        System.out.println( "Report Failed!" );
        
        String[] names =  new String[ publication.getAuthors( ).length ];
        
        for( int i = 0; i < names.length; i ++ )
        {
            names[ i ] = publication.getAuthors( )[ i ].getName( );
        }
        
        String heading = "REJECTED: " + publication.getReport( ).getHeading( );

        Server.getInstance( ).createNewPublication( names, heading, "" );
        Server.getInstance( ).editPublication( heading, errorMessage );
        
        PublicationViewDecorator notification = new PublicationViewDecorator( heading );
        notification.add( new PublicationViewLeaf( publication.getReport( ).getHeading( ) ) );
        
        Server.getInstance( ).setNotification( notification );
        
        publication.rejected( );
        
        Server.getInstance( ).notifyClients( );
    }
}
