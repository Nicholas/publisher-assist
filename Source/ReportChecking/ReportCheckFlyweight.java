package ReportChecking;

import java.util.ArrayList;

import Tools.TypedClassLoader;

/**
 * Control the usage and single instances of the report checker classes
 * 
 * @author Nicholas Welters
 *
 */

public class ReportCheckFlyweight
{
	////////////
	// Types //
	//////////
	// Instance container
	private class SingleInstance
	{
		public String className;
		
		public ReportCheck checker;
	}

	//////////////
	// Members //
	////////////
	// A instance of this to create a singleton of this
	private static ReportCheckFlyweight INSTANCE = new ReportCheckFlyweight( );
	
	// Default class.
	private static final String DEFAULT_CLASS = "SimpleCheck";
	
	// list of created objects that are used
	private ArrayList< SingleInstance > instances;
	
	// Class loader for dynamic class loading
	private TypedClassLoader< ReportCheck > RCLoader;
	
	// list of available classes for the chain
	private String [] avalableChecks;
	
	//////////////
	// Methods //
	////////////
	// Public interface for getting the instance
	public static ReportCheckFlyweight getInstance() 
	{
		return INSTANCE;
	}
	
	// Constructor
	private ReportCheckFlyweight( )
	{
		instances = new ArrayList< ReportCheckFlyweight.SingleInstance >( );
		
		RCLoader = new TypedClassLoader< ReportCheck >( "Binary/Addons/ReportChecking" );
		
		avalableChecks = new String[ RCLoader.getAvalableObjects( ).length + 1 ];
		
		
		for( int i = 0; i < RCLoader.getAvalableObjects( ).length; i++ )
		{
			avalableChecks[ i ] = RCLoader.getAvalableObjects( )[ i ];
		}
		
		avalableChecks[ RCLoader.getAvalableObjects( ).length ] = DEFAULT_CLASS;
		
		//
		int c = 0;
		c++;
	}
	
	// Search for an available object
	private int find( String name )
	{
		for( int i = 0; i < instances.size( ); i++ )
		{
			if( instances.get( i ).className.equals( name ) )
			{
				return i;
			}
		}
		
		return -1;
	}
	
	// Create a new object from an unloaded class
	private void load( String name, ReportCheck reportCheck )
	{
		SingleInstance instance = new SingleInstance( );
		instance.className = name;
		instance.checker = reportCheck;
		
		instances.add( instance );
	}
	
	// Get an instance of a report checker
	public ReportCheck getCheck( String name )
	{
		int index = find( name );
		
		if( index > -1 )
		{
			return instances.get( index ).checker;
		}
		else
		{
			ReportCheck reportCheck;
			
			if( name.equals( DEFAULT_CLASS ) )
			{
				reportCheck = new SimpleCheck( );
				load( DEFAULT_CLASS, reportCheck );
				return reportCheck;
			}
			else
			{
				try
				{
					reportCheck = RCLoader.getClass( name );
					
					load( name, reportCheck );
					
					return reportCheck;
				}
				catch( ClassNotFoundException e )
				{
					e.printStackTrace();
					
					return getCheck( DEFAULT_CLASS );
				}
			}
		}
	}
	
	// Get a list of all the report checkers
	public String [] getAvalableChecks( )
	{
		return avalableChecks;
	}
}
