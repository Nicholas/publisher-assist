package ReportChecking;

import Publication.Publication;

public class ResultListener
{
	private Publication publication;
	
    private AcceptCommand acceptListener;
    private RejectCommand rejectListener;
	
	public ResultListener( Publication publication, AcceptCommand acceptListener, RejectCommand rejectListener )
	{
		this.publication = publication;
        
        this.acceptListener = acceptListener;
        this.rejectListener = rejectListener;
	}
	
	public Publication getPublication( )
	{
		return publication;
	}
	
    public void accept( )
    {
        acceptListener.execute( );
    }

    public void reject( String errorMessage )
    {
        rejectListener.execute( errorMessage );
    }
}
// Acceptance