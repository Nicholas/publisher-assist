package ReportChecking;

import LiveSystem.LiveView;
import Publication.Publication;
import PublicationExternal.PublicationViewComposite;
import PublicationExternal.PublicationViewDecorator;
import PublicationExternal.PublicationViewLeaf;
import Server.Server;

public class Accept implements AcceptCommand
{
    private Publication publication;
    private String parent;
    private LiveView liveView;
    
    public Accept( Publication publication, String parent, LiveView liveView )
    {
        this.publication = publication;
        this.parent = parent;
        this.liveView = liveView;
    }

    @ Override
    public void execute( )
    {
        if( parent != "" )
        {
            // TODO Notify Parent Nods About The Comment
            
            //Get the parents author names;
            String[] names =  Server.getInstance( ).getAuthors( parent );
            
            for( int i = 0; i < names.length; i ++ )
            {
                boolean found = false;
                for( int j = 0; j < publication.getAuthors( ).length; j++ )
                {
                    if( publication.getAuthors( )[ j ].getName( ).equals( names[ i ] ) )
                    {
                        found = true;
                    }
                }
                
                if( ! found )
                {
                    String heading = "COMMENT: " + publication.getReport( ).getHeading( ) + " - " + names[ i ];
    
                    Server.getInstance( ).createNewPublication( new String[]{ names[i] }, heading, "" );
                    Server.getInstance( ).editPublication( heading, publication.getReport( ).getHeading( ) + " has been commented on!" );
                    
                    PublicationViewDecorator notification = new PublicationViewDecorator( heading );
                    PublicationViewDecorator yours = new PublicationViewDecorator( parent );
                    yours.add( new PublicationViewLeaf( publication.getReport( ).getHeading( ) ) );
                    notification.add( yours );
                    
                    Server.getInstance( ).setNotification( notification );
                }
            }
        }
        
        PublicationViewComposite thisLive = new PublicationViewComposite( publication.getReport( ).getHeading( ) );
        liveView.add( parent, thisLive );
        publication.setLive( thisLive );
        
        Server.getInstance( ).notifyClients( );
    }
}
