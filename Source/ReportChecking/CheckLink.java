package ReportChecking;


public class CheckLink implements AcceptCommand
{
	private ReportCheck check;
	private ResultListener result;
	
    public CheckLink( ReportCheck check, ResultListener result )
    {
        this.check = check;
        this.result = result;
    }

    @ Override
    public void execute( )
    {
		check.add( result );
    }
}
