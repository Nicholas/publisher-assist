package Client;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import Server.Server;

public class Client
{
	private AuthorTools authorTools;
	private ReportsPanel reportsPanel;

	private ReportBuilder reportBuilder;
	private PublishTestOptions options;
	
	private MainFrame frame;
	
	public Client( String author )
	{
		options = new PublishTestOptions( author );
		reportBuilder = new ReportBuilder( author, options );
		reportsPanel = new ReportsPanel( author, Server.getInstance( ).getLiveView( ), reportBuilder, options );
		authorTools = new AuthorTools( author, reportBuilder, options );
		
		frame = new MainFrame( "Journal Publication Assistant :: " + author + " ::" );

		JTabbedPane tabs = new JTabbedPane( JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT );

		tabs.addTab( "Author Tools", authorTools );
		tabs.addTab( "Live Reports", reportsPanel );
		
		frame.add( tabs );
		
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setSize( 1100, 768 );
		frame.setVisible( true );
	}

	public void updateData( )
	{
		authorTools.updateData( );
		reportsPanel.updateData( );
	}
}
