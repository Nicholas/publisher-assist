package Client;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
//import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import PublicationExternal.PublicationView;

public class ReportsPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3006657284859102906L;

	private JScrollPane[] reportPanels;
	private JScrollPane reportListPane;
	
	private JList reportList;
	
	private int currentPanel;

	private GridBagLayout layout;
	private GridBagConstraints constraints;
	
	private LinkedList< PublicationView > reports;

	private String author;

	private ReportBuilder reportBuilder;
	private PublishTestOptions options;
	
	private void updateDATA( )
	{
		if( reportPanels != null )
		{
			for( int i = 0; i < reportPanels.length; i++ )
			{
                if( reportPanels[ i ] != null )
                {
                    remove( reportPanels[ i ] );
                }
			}
			
			remove( reportListPane );
		}
		
		reportPanels = new JScrollPane[ reports.size( ) ];
		String[] headings = new String[ reports.size( ) ];
        PublicationView views[] = new PublicationView[ reports.size( ) ];
        reports.toArray( views );

		for( int i = 0; i < views.length; i++ )
		{
			headings[ i ] = views[ i ].getPublication( );

			reportPanels[ i ] = new JScrollPane( new ReportTreePanel( author, views[ i ], reportBuilder, options ) );
			
			reportPanels[ i ].setVisible( false );
			reportPanels[ i ].setHorizontalScrollBarPolicy( ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS );
			reportPanels[ i ].setVerticalScrollBarPolicy( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
			
		}

		
		
		reportList = new JList( headings );
		reportList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
		
		reportListPane = new JScrollPane( reportList );

		reportListPane.setMinimumSize( new Dimension( 230, 50 ) );
		reportListPane.setPreferredSize( new Dimension( 230, 50 ) );
		
		addComponent( reportListPane
				, GridBagConstraints.VERTICAL
				, 0, 0
				, 1, 1 
				, 0, 0.0
				);
	
		currentPanel = -1;
		
	
		for( int i = 0; i < headings.length; i++ )
		{
			addComponent( reportPanels[ i ]
						, GridBagConstraints.BOTH
						, 0, 1
						, 1, 1 
						, 1.0, 1.0 
						);
		}
		
		if( headings.length > 0 )
		{
			reportList.setSelectedIndex( 0 );
			setReportTree( );
		}
	
		reportList.addListSelectionListener( new ReportListSelection( this ) );
	}
	
	public ReportsPanel( String author, LinkedList< PublicationView > reports, ReportBuilder reportBuilder, PublishTestOptions options )
	{
		layout = new GridBagLayout( );
		constraints = new GridBagConstraints( );
		
		setLayout( layout );

		this.reports = reports;
		this.author = author;
		this.reportBuilder = reportBuilder;
		this.options = options;
		
		updateDATA( );
	}
	
	private void setReportTree( )
	{
		
		if( currentPanel != -1 )
		{
			reportPanels[ currentPanel ].setVisible( false );
		}
		currentPanel = reportList.getSelectedIndex( );
		
		reportPanels[ currentPanel ].setVisible( true );
		validate( );
	}

	
	private void addComponent( JComponent component
							 , int fill
							 , int row, int column
							 , int width, int height
							 , double weightX, double weightY
							 )
	{
		constraints.fill = fill;
		
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		
		constraints.weightx = weightX;
		constraints.weighty = weightY;
		
		layout.setConstraints( component, constraints );
		add( component );
	}
	
	private class ReportListSelection implements ListSelectionListener
	{
		private ReportsPanel thePanelToChange;
		
		public ReportListSelection( ReportsPanel thePanelToChange )
		{
			this.thePanelToChange = thePanelToChange;
		}

		@ Override
		public void valueChanged( ListSelectionEvent e )
		{
			// TODO Auto-generated method stub
			thePanelToChange.setReportTree( );
		}
		
	}

	public void updateData( )
	{
		updateDATA( );
	}
}
