package Client;

import javax.swing.JOptionPane;

import Server.Server;

public class Application
{
	// Program entry
	public static void main( String[] args )
	{
		Server.getInstance( ).prototypeInit();

		///////////////////////////////////////////////////////////
		// - CLIENT APPLICATION ------------------------------- //
		/////////////////////////////////////////////////////////
		Server.getInstance( ).connent( "Default" );

        int answer = JOptionPane.showConfirmDialog(
            null,
            "Do want to create a second test profile?\n",
            "Create A New Profile?",
            JOptionPane.YES_NO_OPTION
        );

		if( answer == JOptionPane.YES_OPTION )
		{
			String authorName = "";
			
			while( authorName.equals( "" ) )
			{
				authorName = JOptionPane.showInputDialog( "Please Enter A Name." );
				
				if( authorName == null )
				{
					break;
				}
			}
			
			if( authorName != null )
			{
				Server.getInstance( ).connent( authorName );
			}
			
		}
		
		///////////////////////////////////////////////////////////
		// - SERVER CHANGE ------------------------------------ //
		/////////////////////////////////////////////////////////
		Server.getInstance( ).randomChange( );

		System.out.println( "Exit Main" );
	}
}
