package Client;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import Server.Server;

public class ReportBuilder extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8965664879774569315L;
	
	private JLabel reportHeading;
	private JTextArea reportText;
	
	private GridBagLayout layout;
	private GridBagConstraints constraints;
	
	private String author;
	private PublishTestOptions publishOptions;
	
	public ReportBuilder( String author, PublishTestOptions publishOptions )
	{
		super( author + " - Document Editor" );
		
		this.author = author;
		this.publishOptions = publishOptions;
		
		layout = new GridBagLayout( );
		constraints = new GridBagConstraints( );
		
		setLayout( layout );
		
		JLabel headingLable = new JLabel( "      Heading: " );
		JLabel textLable    = new JLabel( "  Document: " );
		
		reportHeading = new JLabel( );
		reportHeading.setFont( new Font( Font.SANS_SERIF, Font.BOLD, 14 ) );
		
		reportText = new JTextArea( );

		addComponent( headingLable
					, GridBagConstraints.VERTICAL
					, 0, 0
					, 1, 1 
					, 0.0, 0.0 
					);
		
		addComponent( textLable
					, GridBagConstraints.VERTICAL
					, 1, 0
					, 1, 1 
					, 0.0, 0.0 
					);
		
		addComponent( new JPanel( )
					, GridBagConstraints.VERTICAL
					, 2, 0
					, 1, 5 
					, 0.0, 0.0 
					);

		
		addComponent( reportHeading
					, GridBagConstraints.HORIZONTAL
					, 0, 1
					, 5, 1 
					, 1.0, 0.0 
					);
		
		addComponent( new JScrollPane( reportText )
					, GridBagConstraints.BOTH
					, 1, 1
					, 5, 3
					, 1.0, 1.0 
					);
		
		addComponent( new JLabel( "By " + this.author )
					, GridBagConstraints.BOTH
					, 4, 1
					, 5, 1
					, 0.0, 0.0 
					);
					
		

		JPanel resetP = new JPanel( );
		JPanel saveP = new JPanel( );
		JPanel sendP = new JPanel( );
		JPanel cancelP = new JPanel( );

		JButton reset = new JButton( "Reset" );
		JButton save = new JButton( "Save" );
		JButton send = new JButton( "Publish" );
		JButton cancel = new JButton( "Cancel" );

		reset.addActionListener( new ResetAction( ) );
		cancel.addActionListener( new CancelAction( ) );

		save.addActionListener( new SaveAction( ) );
		send.addActionListener( new SendAction( ) );

		resetP.add(  reset );
		saveP.add( save );
		sendP.add( send );
		cancelP.add( cancel );
		
		addComponent( new JPanel( )
					, GridBagConstraints.HORIZONTAL
					, 5, 1
					, 1, 1 
					, 1.0, 0.0 
					);
		
		addComponent( resetP
					, GridBagConstraints.NONE
					, 5, 2
					, 1, 1 
					, 0.0, 0.0 
					);
		
		addComponent( saveP
					, GridBagConstraints.NONE
					, 5, 3
					, 1, 1 
					, 0.0, 0.0 
					);
		
		addComponent( sendP
					, GridBagConstraints.NONE
					, 5, 4
					, 1, 1 
					, 0.0, 0.0 
					);
		
		addComponent( cancelP
					, GridBagConstraints.NONE
					, 5, 5
					, 1, 1 
					, 0.0, 0.0 
					);

		setSize( 800, 600 );
	}
	
	private String defaultText;

	public void editPublication( String reportTitle, String text )
	{
		defaultText = text;
		
		reportHeading.setText( reportTitle );
		reportText.setText( defaultText );
		
		setVisible( true );
	}
	
	private void resetText( )
	{
		reportText.setText( defaultText );
	}


	private void addComponent( JComponent component
							 , int fill
							 , int row, int column
							 , int width, int height
							 , double weightX, double weightY
							 )
	{
		constraints.fill = fill;
		
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		
		constraints.weightx = weightX;
		constraints.weighty = weightY;
		
		layout.setConstraints( component, constraints );
		add( component );
	}
	
	private class CancelAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			setVisible( false );
		}
	}
	
	private class ResetAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			resetText( );
		}
	}
	
	private class SaveAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			setVisible( false );
			
			boolean tryAgain = true;
			while( ( ! Server.getInstance( ).editPublication( reportHeading.getText( ), reportText.getText( ) ) ) && tryAgain )
			{
				if( JOptionPane.showConfirmDialog( null, "Error saving text.\nTry Again?", "Save Error", JOptionPane.YES_NO_OPTION ) == JOptionPane.NO_OPTION )
				{
					tryAgain= false;
				}
			}
		}
	}
	
	private class SendAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			setVisible( false );
			
			boolean tryAgain = true;
			while( ( ! Server.getInstance( ).editPublication( reportHeading.getText( ), reportText.getText( ) ) ) && tryAgain )
			{
				if( JOptionPane.showConfirmDialog( null, "Error saving text.\nTry Again?", "Save Error", JOptionPane.YES_NO_OPTION ) == JOptionPane.NO_OPTION )
				{
					tryAgain= false;
				}
			}
			
			publishOptions.publishReport( reportHeading.getText( ) );
		}
	}
}
