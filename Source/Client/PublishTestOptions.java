package Client;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import Server.Server;

public class PublishTestOptions extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1436638718019842621L;
	//private String author;
	private JCheckBox[] checkBoxes;
	
	public PublishTestOptions( String author )
	{
		super( author + " - Test Selection" );
		//this.author = author;
		
		String[] tests = Server.getInstance( ).getChecks( );
		
		JPanel checkGroup = new JPanel( );
        checkGroup.setLayout( new BoxLayout( checkGroup, BoxLayout.PAGE_AXIS ) );
		
		if( tests.length > 0 )
		{
			checkBoxes = new JCheckBox[ tests.length ];
	
			for( int i = 0; i < tests.length; i++ )
			{
				checkBoxes[ i ] = new JCheckBox( tests[ i ] );
				
				checkGroup.add( checkBoxes[ i ] );
			}
		}
		
		setSize( 400, 400 );

		JButton acceptButton = new JButton( "Ok" );
		acceptButton.addActionListener( new PublishAction( ) );
		
		JButton cancelButton = new JButton( "Cancel" );
		cancelButton.addActionListener( new CancelAction( ) );

		//FlowLayout layout = new FlowLayout( );
		//layout.setAlignment(FlowLayout.LEFT);
		
		//setLayout( layout );
        BoxLayout layout = new BoxLayout( getContentPane( ), BoxLayout.PAGE_AXIS );
        setLayout( layout );

		add( checkGroup, Component.RIGHT_ALIGNMENT );
		add( acceptButton );
        add(Box.createRigidArea(new Dimension(10, 0)));
		add( cancelButton );
	}
	public String publication = null;
	
	public void publishReport( String publication )
	{
		this.publication = publication;
		setVisible( true );
	}
	
	private class PublishAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			int countSelected = 0;
			
			for( int i = 0; i < checkBoxes.length; i++ )
			{
				if( checkBoxes[ i ].isSelected( ) )
				{
					countSelected++;
				}
			}
			
			String[] selection = new String[ countSelected ];
			countSelected = 0;

			for( int i = 0; i < checkBoxes.length; i++ )
			{
				if( checkBoxes[ i ].isSelected( ) )
				{
					selection[ countSelected ] = checkBoxes[ i ].getText( );
					countSelected++;
				}
			}
			
			Server.getInstance( ).publishPublication( publication, selection );
			
			setVisible( false );
			
		}
	}
	
	private class CancelAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			// TODO Auto-generated method stub
			
			setVisible( false );
		}
	}
}
