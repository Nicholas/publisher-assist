package Client;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JPanel;

import PublicationExternal.PublicationView;

public class ReportTreePanel extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7586624730128381883L;
	
	private PublicationView report;
	
	private JPanel innerPanel;

	private String author;

	private ReportBuilder reportBuilder;
	private PublishTestOptions options;

	public ReportTreePanel( String author, PublicationView view, ReportBuilder reportBuilder, PublishTestOptions options )
	{
		this.report = view;
		this.author = author;
		this.reportBuilder = reportBuilder;
		this.options = options;
		
		FlowLayout layout = new FlowLayout( );
		layout.setAlignment( FlowLayout.LEFT );
		
		setLayout( layout );
		
		
		innerPanel = new JPanel( );
		
		
		int totalNumberOfPublications = caculateNumberOfChildren( );
		innerPanel.setLayout( new GridLayout( totalNumberOfPublications, 1, 0, 0 ) );
		
		buildView( );
		
		add( innerPanel );
	}

	private JPanel ReportTreeIndentation( int i , PublicationView report )
	{
		JPanel temp = new JPanel( );
		
		int gap = 20;
		
		temp.add( Box.createHorizontalStrut( i * gap ) );
		
		ReportPanel panel = new ReportPanel( author, report, reportBuilder, options );

		panel.setPreferredSize( new Dimension( 800 - ( i * gap ), 200 ) );
		
		temp.add( panel );
		
		return temp;
	}
	
	private int caculateNumberOfChildren( )
	{
		return 1 + caculateNumberOfChildren( report );
	}
	
	private int caculateNumberOfChildren( PublicationView report )
	{
		int count = report.size( );
		
		for( int i = 0; i < report.size( ); i++ )
		{
			count += caculateNumberOfChildren( report.getChild( i ) );
		}
		
		return count;
	}
	
	private void buildView( )
	{
		buildView( report, 0 );
	}
	
	private void buildView( PublicationView report, int depth )
	{
		innerPanel.add( ReportTreeIndentation( depth, report ) );
		
		for( int i = 0; i < report.size( ); i++ )
		{
			buildView( report.getChild( i ), depth + 1 );
		}
	}
}
