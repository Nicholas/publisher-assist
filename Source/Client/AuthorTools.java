package Client;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import Server.Server;

public class AuthorTools extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6890870403675999998L;

	private GridBagLayout layout;
	private GridBagConstraints constraints;
	
	
	private ReportsPanel[] reportPanels;

	private ReportBuilder reportBuilder;
	//private PublishTestOptions options;
	
	private String author;

	
	public AuthorTools( String author, ReportBuilder reportBuilder, PublishTestOptions options )
	{
		this.reportBuilder = reportBuilder;
		this.author = author;
		//this.options = options;
		
		
		layout = new GridBagLayout( );
		constraints = new GridBagConstraints( );
		
		setLayout( layout );
		
		reportPanels = new ReportsPanel[ 4 ];

		reportPanels[ 0 ] = new ReportsPanel( author, Server.getInstance( ).getDrafts( author ), reportBuilder, options );
		reportPanels[ 1 ] = new ReportsPanel( author, Server.getInstance( ).getPending( author ), reportBuilder, options );
		reportPanels[ 2 ] = new ReportsPanel( author, Server.getInstance( ).getLive( author ), reportBuilder, options );
        reportPanels[ 3 ] = new ReportsPanel( author, Server.getInstance( ).getNotifications( author ), reportBuilder, options );
		
		
		JLabel authorLable = new JLabel( "  Author: " + author );
		
		JButton newDocument = new JButton( "Create New Report" );
		JPanel newDocP = new JPanel( );
		newDocP.add(  newDocument );
		
		newDocument.addActionListener( new CreateNewAction( ) );
		
		JTabbedPane tabs = new JTabbedPane( JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT );

		tabs.addTab( "Drafts", reportPanels[ 0 ] );
		tabs.addTab( "Pending", reportPanels[ 1 ] );
		tabs.addTab( "Published", reportPanels[ 2 ] );
        tabs.addTab( "Notifications", reportPanels[ 3 ] );
		
		//add( tabs );
		
		addComponent( authorLable
					, GridBagConstraints.HORIZONTAL
					, 0, 0
					, 1, 1 
					, 1.0, 0.0 
					);
		
		addComponent( newDocP
					, GridBagConstraints.HORIZONTAL
					, 0, 1
					, 1, 1 
					, 0.0, 0.0 
					);
		
		addComponent( tabs
					, GridBagConstraints.BOTH
					, 1, 0
					, 2, 1 
					, 1.0, 1.0 
					);
	}


	private void addComponent( JComponent component
							 , int fill
							 , int row, int column
							 , int width, int height
							 , double weightX, double weightY
							 )
	{
		constraints.fill = fill;
		
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		
		constraints.weightx = weightX;
		constraints.weighty = weightY;
		
		layout.setConstraints( component, constraints );
		add( component );
	}
	
	private class CreateNewAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			String[] authors = { author };
			
			String reportTitle;
			String question = "Enter A Document Title";
			
			do
			{
				do
				{
					reportTitle= "";
					
					reportTitle = JOptionPane.showInputDialog( question );
					
					question = "Please Enter A Document Title";
					
					if( reportTitle == null )
					{
						return;
					}
				}
				while( reportTitle.equals( "" ) );
				
				question = "The title " + reportTitle + " is already in use.\nPlease Enter A New Document Title";
			}
			while( ! Server.getInstance( ).createNewPublication( authors, reportTitle, "" ) );
			
			reportBuilder.editPublication( reportTitle, "" );
			
			//Server.getInstance( ).getReport( author, reportTitle );
		}
		
	}

	public void updateData( )
	{
		for( int i = 0; i < reportPanels.length; i++ )
		{
			reportPanels[ i ].updateData( );
		}
	}
}
