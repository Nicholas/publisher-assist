package Client;

import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.SwingUtilities;

public class MainFrame extends JFrame
{
	private static final long serialVersionUID = 1841497286964077772L;
	/**
	 * 
	 */
	private UIManager.LookAndFeelInfo looks[];
	
	private JRadioButtonMenuItem [] lookAndFeelRadioButtonMenuItem;
	
	public MainFrame( String s )
	{
		super(s);
		
		//---:: MENU BAR ::----------------------------------
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar( menuBar );
		
		//   :: MENU ::
		JMenu fileMenu = new JMenu( "File" );
		fileMenu.setMnemonic( 'F' );
		menuBar.add( fileMenu );
		
		//      :: MENU ITEM :: ( File )
		JMenuItem exitItem = new JMenuItem( "Exit" );
		exitItem.setMnemonic( 'x' );
		exitItem.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent event )
				{
					System.exit(0);
				}
			}
		);
		fileMenu.add( exitItem );
		
		JMenu optionsMenu = new JMenu( "Options" );
		optionsMenu.setMnemonic( 'O' );
		menuBar.add( optionsMenu );
			
		//      :: MENU :: ( Options )
		JMenu lookAndFeelMenu = new JMenu ( "Look & Feel" );
		lookAndFeelMenu.setMnemonic( 'L' );
		optionsMenu.add( lookAndFeelMenu );
		looks = UIManager.getInstalledLookAndFeels();
			
		//      :: MENU ITEM :: ( Look & Feel )
		ButtonGroup lookAndFeelButtonGroup = new ButtonGroup();
		lookAndFeelRadioButtonMenuItem = new JRadioButtonMenuItem [ looks.length ];
		
		for ( int i = 0; i < looks.length; i++ )
		{
			lookAndFeelRadioButtonMenuItem[i] = new JRadioButtonMenuItem( looks[i].getName( ) );
			lookAndFeelRadioButtonMenuItem[i].addItemListener( new lookAndFeelItemListener() );
			lookAndFeelButtonGroup.add( lookAndFeelRadioButtonMenuItem[i] );
			lookAndFeelMenu.add( lookAndFeelRadioButtonMenuItem[i] );
		}
		lookAndFeelRadioButtonMenuItem[0].setSelected( true );
			
		//      :: MENU ITEM :: ( Options )
		JMenu helpMenu = new JMenu( "Help" );
		helpMenu.setMnemonic( 'H' );
		menuBar.add( helpMenu );
			
		//      :: MENU ITEM :: ( Help )
		JMenuItem aboutItem = new JMenuItem( "About" );
		aboutItem.setMnemonic( 'o' );
		aboutItem.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent event )
				{
					JOptionPane.showMessageDialog( MainFrame.this, "About information\n\nStd Frame v1.1\nReporter Support Framework\nThis is a concept reporting and commenting system.\n\nAuthor: Nicholas Welters", "About", JOptionPane.PLAIN_MESSAGE );
				}
			}
		);
		helpMenu.add( aboutItem );
		//---:: MENU BAR ::----------------------------------
		
	}
	
	//		>>> Methods >>>		//
	//---:: Change Look And Feel ::----------------------------------
	public void changeLookAndFeel( int value )
	{
		try
		{
			UIManager.setLookAndFeel( looks[ value ].getClassName() );
			SwingUtilities.updateComponentTreeUI( this );
		}
		catch ( Exception exception )
		{
			exception.printStackTrace();
		}
	}
	//---:: look And Feel Item Listener ::----------------------------------
	private class lookAndFeelItemListener implements ItemListener
	{
		public void itemStateChanged ( ItemEvent event )
		{
			for ( int i = 0; i < lookAndFeelRadioButtonMenuItem.length; i++ )
			{
				if ( lookAndFeelRadioButtonMenuItem[i].isSelected() )
				{
					changeLookAndFeel( i );
				}
			}
		}
	}
}