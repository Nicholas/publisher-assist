package Client;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import PublicationExternal.PublicationView;
import PublicationInternal.PublicationState;
import Server.Server;

public class ReportPanel extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7803121602901134044L;

	private GridBagLayout layout;
	private GridBagConstraints constraints;

	private String author;
	private PublicationView report;
	private ReportBuilder reportBuilder;
	private PublishTestOptions publishOptions;
	
	public ReportPanel( String author, PublicationView report, ReportBuilder reportBuilder, PublishTestOptions publishOptions )
	{
		super( );
		this.author = author;
		this.report = report;
		this.reportBuilder = reportBuilder;
		this.publishOptions = publishOptions;
		
		layout = new GridBagLayout( );
		constraints = new GridBagConstraints( );
		
		setLayout( layout );
		
		JPanel headingPanel = new JPanel( );
		
		headingPanel.setLayout( new FlowLayout( ) );
		
		JLabel headingLable = new JLabel( " " + report.getPublication( ) );
		
		headingLable.setFont( new Font( Font.SANS_SERIF, Font.BOLD, 14 ) );
		
		addComponent( headingLable
					, GridBagConstraints.HORIZONTAL
					, 0, 0
					, 4, 1 
					, 1.0, 0.0 
					);
		
		String autherString = "  Author";

		String[] authorNames = Server.getInstance( ).getAuthors( report.getPublication( ) );
		
		if( authorNames.length > 0 )
		{
			if( authorNames.length > 1 )
			{
				autherString += "s";
			}
			
			autherString += ": " + authorNames[ 0 ];
			
			for( int i = 1; i < authorNames.length; i++ )
			{
				autherString += ", " + authorNames[ i ];
			}
		}
		
		JLabel AuthorsLable = new JLabel( autherString );

		
		addComponent( AuthorsLable
					, GridBagConstraints.HORIZONTAL
					, 1, 0
					, 4, 1 
					, 1.0, 0.0 
					);


		String[] functionNames = Server.getInstance( ).getFunctions( report.getPublication( ) );
		
		int i = 0;
		for( i = 0; i < functionNames.length; i++ )
		{
			JPanel commentPanel = new JPanel( );
			JButton commentButton = new JButton( functionNames[ i ] );

			if( functionNames[ i ] == PublicationState.FUNCTION_COMMENT )
			{
				
				commentButton.addActionListener( new CommentAction( ) );
			}
			else if( functionNames[ i ] == PublicationState.FUNCTION_EDIT )
			{
				
				commentButton.addActionListener( new EditAction( ) );
			}
			else if( functionNames[ i ] == PublicationState.FUNCTION_DELETE )
			{
				
				commentButton.addActionListener( new DeleteAction( ) );
			}
			else if( functionNames[ i ] == PublicationState.FUNCTION_PUBLISH )
			{
				
				commentButton.addActionListener( new PublishAction( ) );
			}
			
			commentPanel.setLayout( new FlowLayout( ) );
			
			commentPanel.add( commentButton );
			
			addComponent( commentButton
						, GridBagConstraints.HORIZONTAL
						, 0, 4 + i
						, 1, 2 
						, 0.0, 0.0 
						);
		}
		
		String text = Server.getInstance( ).getReport( report.getPublication( ) );
		
		JTextArea reportTextArea = new JTextArea( text );
		reportTextArea.setEditable( false );

		reportTextArea.setLineWrap( true );
		
		addComponent( new JScrollPane( reportTextArea )
					, GridBagConstraints.BOTH
					, 2, 0
					, 5 + i, 3 
					, 1.0, 1.0 
					);
	}

	private void addComponent( JComponent component
							 , int fill
							 , int row, int column
							 , int width, int height
							 , double weightX, double weightY
							 )
	{
		constraints.fill = fill;
		
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		
		constraints.weightx = weightX;
		constraints.weighty = weightY;
		
		layout.setConstraints( component, constraints );
		add( component );
	}
	
	private class EditAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			reportBuilder.editPublication( report.getPublication( ), Server.getInstance( ).getReport( report.getPublication( ) ) );
		}
	}
	
	private class PublishAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			publishOptions.publishReport( report.getPublication( ) );
		}
	}
	
	private class DeleteAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			Server.getInstance( ).deletePublication( report.getPublication( ) );
		}
	}
	
	private class CommentAction implements ActionListener
	{
		@ Override
		public void actionPerformed( ActionEvent e )
		{
			//TODO
			reportBuilder.editPublication( Server.getInstance( ).commentOnPublication( report.getPublication( ), new String[]{ author } ), "" );
		}
	}
}
