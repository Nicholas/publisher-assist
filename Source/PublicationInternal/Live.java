package PublicationInternal;

import LiveSystem.LiveView;
import Publication.Publication;
import PublicationExternal.PublicationView;
import Server.Server;



public class Live extends PublicationState
{
	private Publication publication;
	private PublicationView thisLive;

	public Live( Publication publication, PublicationView thisLive )
	{
		super( new String[]{ FUNCTION_COMMENT } );
		
		this.publication = publication;
		this.thisLive = thisLive;;
	}

	@ Override
	public boolean delete( )
	{
		// TODO Auto-generated method stub
		return false;
	}

	@ Override
	public boolean publish( String[] checks, LiveView liveView )
	{
		// TODO Auto-generated method stub
		return false;
	}

	@ Override
	public boolean edit( String text )
	{
		// TODO Auto-generated method stub
		return false;
	}

	@ Override
	public String comment( String text, String[] authorNames )
	{
		String commentTitle = "Re [" + thisLive.size( ) + "]: " + publication.getReport( ).getHeading( );
		
		int count = 0;
		
		while( ! Server.getInstance( ).createNewPublication( authorNames, commentTitle, thisLive.getPublication( ) ) )
		{
			commentTitle = "Re [" + ( thisLive.size( ) + count ) + "]: " + publication.getReport( ).getHeading( );
			count++;
		}
		
		return commentTitle;
	}
}
