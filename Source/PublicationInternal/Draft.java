package PublicationInternal;

import LiveSystem.LiveView;
import Publication.Publication;
import ReportChecking.Accept;
import ReportChecking.AcceptanceTestBuilder;
import ReportChecking.Reject;
import Server.Server;

public class Draft extends PublicationState
{
	private Publication publication;
	
	private String parent;
	
	public Draft( Publication publication, String parent )
	{
		super( new String[]{ FUNCTION_EDIT, FUNCTION_PUBLISH, FUNCTION_DELETE } );
		
		this.publication = publication;
		this.parent = parent;
	}

	@ Override
	public boolean delete( )
	{
		for( int i = 0; i < publication.getAuthors( ).length; i++ )
		{
			publication.getAuthors( )[ i ].remove( publication.getReport( ).getHeading( ) );
		}
		
		return true;
	}

	@ Override
	public boolean publish( String[] checks, LiveView liveView )
	{
		publication.setPending( );
		
		Server.getInstance( ).notifyClients( );

		Accept acceptFunction = new Accept( publication, parent, liveView );
		Reject rejectFunction = new Reject( publication );
		
		AcceptanceTestBuilder.getInstance( ).buildAcceptanceTest( publication, checks, acceptFunction, rejectFunction );
		
		return true;
	}

	@ Override
	public boolean edit( String text )
	{
		publication.getReport( ).setText( text );
		
		return true;
	}

	@ Override
	public String comment( String text, String[] authorNames )
	{
		return "";
	}
}
