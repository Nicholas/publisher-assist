package PublicationInternal;

import LiveSystem.LiveView;
import Publication.Publication;

public class Notifiaction extends PublicationState
{
	private Publication publication;

	public Notifiaction( Publication publication )
	{
		super( new String[]{ FUNCTION_DELETE }  );
		
		this.publication = publication;
	}

	@ Override
	public boolean delete( )
	{
		for( int i = 0; i < publication.getAuthors( ).length; i++ )
		{
			publication.getAuthors( )[ i ].removeNotification( publication.getReport( ).getHeading( ) );
		}
		
		return true;
	}

	@ Override
	public boolean publish( String[] checks, LiveView liveView )
	{
		return false;
	}

	@ Override
	public boolean edit( String text )
	{
		return false;
	}

	@ Override
	public String comment( String text, String[] authorNames )
	{
		return "";
	}
}
