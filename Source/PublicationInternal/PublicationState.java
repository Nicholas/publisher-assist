package PublicationInternal;

import LiveSystem.LiveView;

public abstract class PublicationState
{
	public static final String FUNCTION_DELETE = "Delete";
	public static final String FUNCTION_PUBLISH = "Publish";
	public static final String FUNCTION_EDIT = "Edit";
	public static final String FUNCTION_COMMENT = "Comment";
	
	private String[] activeFunctions;
	
	
	public PublicationState( String[] functions )
	{
		setActiveFunctions( functions );
	}
	
	
	private void setActiveFunctions( String[] activeFunctions )
	{
		this.activeFunctions = activeFunctions;
	}
	
	public String[] getFunctions( )
	{
		return activeFunctions;
	}
	
	
	abstract public boolean  delete( );
	abstract public boolean publish( String[] checks, LiveView liveView );
	
	abstract public boolean    edit( String text );
	abstract public String comment( String text, String[] authorNames );
}
