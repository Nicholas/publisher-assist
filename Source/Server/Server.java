package Server;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import Author.Author;
import Author.AuthorCreator;
import Author.AuthorKey;
import Client.Client;
import LiveSystem.LiveView;
import Publication.Publication;
import Publication.PublicationCreator;
import Publication.PublicationKey;
import PublicationExternal.PublicationView;
import PublicationExternal.PublicationViewDecorator;
import Report.Report;
import Report.ReportCreator;
import Report.ReportKey;
import ReportChecking.AcceptanceTestBuilder;
import Tools.Flyweight;

public class Server
{
	private static final Server INSTANCE = new Server( );

	private Flyweight< Author, AuthorKey > authors;
	private Flyweight< Report, ReportKey > reports;
	private Flyweight< Publication, PublicationKey > publications;
	
	private Map< String, Client > clients;
	
	private LiveView liveView;
	
	private Server( )
	{
		// Flyweights
		Author defaultAuthor = new Author( "Default" );
		authors = new Flyweight< Author, AuthorKey >( defaultAuthor, new AuthorCreator( ) );

		Report defaultReport = new Report( "Default" );
		defaultReport.setText( "ERROR - This is default text!" );
		reports = new Flyweight< Report, ReportKey >( defaultReport, new ReportCreator( ) );
		
		Publication defaultPublication =  new Publication( new Author[]{ defaultAuthor }, reports.get( new ReportKey( "Default" ) ) );
		publications = new Flyweight< Publication, PublicationKey >( defaultPublication, new PublicationCreator( ) );
		
		// Observers
		clients = new Hashtable< String, Client >( );
		
		// Tool
		liveView = new LiveView( );
	}
	
	public static Server getInstance( )
	{
		return INSTANCE;
	}
	
	public Client connent( String authorName )
	{
		Author author = authors.get( new AuthorKey( authorName ) );
		
		clients.put( authorName, new Client( author.getName( ) ) );
		
		return clients.get( authorName );
	}

	public boolean createNewPublication( String[] authorNames, String publication, String parent )
	{
		Author[] authenticAuthors = authenticateAuthors(authorNames);
		
		ReportKey report = new ReportKey( publication );
		if( reports.containsKey( report ) )
		{
			return false;
		}
		else
		{
			newPublication( authenticAuthors, reports.get( report ), parent );
			
			return true;
		}
	}
	
	private void newPublication( Author[] authenticAuthors, Report report, String parent )
	{
		PublicationKey publicationKey = new PublicationKey( authenticAuthors, report );
		
		if( ! publications.containsKey( publicationKey ) )
		{
			publications.get( publicationKey ).setDraft( parent );
		}
		
		notifyClients( );
	}

	public boolean editPublication( String publication, String text )
	{
		ReportKey key = new ReportKey( publication );
		
		if( reports.containsKey( key ) )
		{
			reports.get( key ).setText( text );
			
			notifyClients( );
			
			return true;
		}
		
		return false;
	}

	public boolean publishPublication( String publication, String[] checks )
	{
		ReportKey reportKey = new ReportKey( publication );
		
		if( reports.containsKey( reportKey ) )
		{
			PublicationKey publicationKey = new PublicationKey( null, reports.get( reportKey ) );
			
			if( publications.containsKey( publicationKey ) )
			{
				boolean success = publications.get( publicationKey ).publish( checks, liveView );
				
				//notifyClients( );
				
				return success;
			}
		}
		
		return false;
	}

	public boolean deletePublication( String publication )
	{
		ReportKey reportKey = new ReportKey( publication );
		
		if( reports.containsKey( reportKey ) )
		{
			PublicationKey publicationKey = new PublicationKey( null, reports.get( reportKey ) );
			
			if( publications.containsKey( publicationKey ) )
			{
				if( publications.get( publicationKey ).delete( ) )
				{
					publications.remove( publicationKey );
					
					notifyClients( );
					
					return true;
				}
			}
		}
		
		
		return false;
	}

	public String commentOnPublication( String publication, String[] authorNames )
	{
		ReportKey reportKey = new ReportKey( publication );
		
		if( reports.containsKey( reportKey ) )
		{
			PublicationKey publicationKey = new PublicationKey( null, reports.get( reportKey ) );
			
			if( publications.containsKey( publicationKey ) )
			{
				return publications.get( publicationKey ).comment( authorNames );
			}
		}
		
		return "";
	}

	public void setNotification( PublicationViewDecorator publication )
	{
		ReportKey reportKey = new ReportKey( publication.getPublication( ) );
		
		if( reports.containsKey( reportKey ) )
		{
			PublicationKey publicationKey = new PublicationKey( null, reports.get( reportKey ) );
			
			if( publications.containsKey( publicationKey ) )
			{
				publications.get( publicationKey ).setNotifiction( publication );
			}
		}
	}
	
	public String[] getChecks( )
	{
		return AcceptanceTestBuilder.getInstance( ).getAcceptanceTests( );
	}
	
	synchronized public void notifyClients( )
	{
		Iterator< String > iter = clients.keySet( ).iterator( );
		
		while( iter.hasNext( ) )
		{
			clients.get( iter.next( ) ).updateData( );
		}
	}
	
	private Author[] authenticateAuthors(String[] authorNames)
	{
		int authorCount = 0;
		
		for( int i = 0; i < authorNames.length; i++ )
		{
			if( authors.containsKey( new AuthorKey( authorNames[ i ] ) ) )
			{
				authorCount++;
			}
		}
		
		Author[] newAuthors = new Author[ authorCount ];
		authorCount = 0;
		
		for( int i = 0; i < authorNames.length; i++ )
		{
			AuthorKey authorKey = new AuthorKey( authorNames[ i ] );
			
			if( authors.containsKey( authorKey ) )
			{
				newAuthors[ authorCount ] = authors.get( authorKey );
				authorCount++;
			}
		}
		
		
		return newAuthors;
	}

	public LinkedList< PublicationView > getLiveView( )
	{
		return liveView.getLiveView( );
	}

	public LinkedList< PublicationView > getNotifications( String authorName )
	{
		AuthorKey key = new AuthorKey( authorName );
		
		if( authors.containsKey( key ) )
		{
			return authors.get( new AuthorKey( authorName ) ).getNotifications( );
		}
		else
		{
			return new LinkedList< PublicationView >( );
		}
	}

	public LinkedList< PublicationView > getDrafts( String authorName )
	{
		AuthorKey key = new AuthorKey( authorName );
		
		if( authors.containsKey( key ) )
		{
			return authors.get( new AuthorKey( authorName ) ).getDrafts( );
		}
		else
		{
			return new LinkedList< PublicationView >( );
		}
	}

	public LinkedList< PublicationView > getPending( String authorName )
	{
		AuthorKey key = new AuthorKey( authorName );
		
		if( authors.containsKey( key ) )
		{
			return authors.get( new AuthorKey( authorName ) ).getPending( );
		}
		else
		{
			return new LinkedList< PublicationView >( );
		}
	}

	public LinkedList< PublicationView > getLive( String authorName )
	{
		AuthorKey key = new AuthorKey( authorName );
		
		if( authors.containsKey( key ) )
		{
			return authors.get( new AuthorKey( authorName ) ).getLive( );
		}
		else
		{
			return new LinkedList< PublicationView >( );
		}
	}

	public String[] getAuthors( String report )
	{
		String[] authorNames = { };
		
		ReportKey reportKey = new ReportKey( report );
		if( reports.containsKey( reportKey ) )
		{
			PublicationKey publicationKey = new PublicationKey( null, reports.get( new ReportKey( report ) ) );
		
			if( publications.containsKey( publicationKey ) )
			{
				authorNames = new String[ publications.get( publicationKey ).getAuthors( ).length ];
				
				for( int i = 0; i < authorNames.length; i++ )
				{
					authorNames[ i ] = publications.get( publicationKey ).getAuthors( )[ i ].getName( );
				}
			}
		}
		
		return authorNames;
	}

	public String[] getFunctions( String report )
	{
		String[] functionNames = { };
		
		ReportKey reportKey = new ReportKey( report );
		if( reports.containsKey( reportKey ) )
		{
			PublicationKey publicationKey = new PublicationKey( null, reports.get( new ReportKey( report ) ) );
		
			if( publications.containsKey( publicationKey ) )
			{
				functionNames = new String[ publications.get( publicationKey ).getFunctions( ).length ];
				
				for( int i = 0; i < functionNames.length; i++ )
				{
					functionNames[ i ] = publications.get( publicationKey ).getFunctions( )[ i ];
				}
			}
		}
		
		return functionNames;
	}

	public String getReport( String report )
	{
		String text = "";
		
		ReportKey reportKey = new ReportKey( report );
		if( reports.containsKey( reportKey ) )
		{
			PublicationKey publicationKey = new PublicationKey( null, reports.get( new ReportKey( report ) ) );
		
			if( publications.containsKey( publicationKey ) )
			{
				text = publications.get( publicationKey ).getReport( ).getText( );
			}
		}
		
		return text;
	}


    /**
     * This creates a few sample data for prototype testing.
     */
	public synchronized void prototypeInit()
	{
		String[] authorNames = { "Default" };
		
		
		////////////////////////////////////////////////////////////////
		// Create Publications                                       //
		//////////////////////////////////////////////////////////////
		String essay = "\n\n" +
                "Hey this is my test string to see it work.\n\n" +
                "This is a relatively long single line string to see what happens when resizing the window.\n" +
                "\n--------------------\n" +
                "\n--------------------\n" +
                "Some more sample text.";
		
		for( int i = 0; i < 5; i++ )
		{
			String reportHeading = "Sample Report Draft { " + i + " }";
			if( createNewPublication( authorNames, reportHeading, "" ) )
			{
				editPublication( reportHeading, reportHeading + " Text " + essay );
			}

			reportHeading = "Live Report Set {A} - " + i;
			if( createNewPublication( authorNames, reportHeading, "" ) )
			{
				editPublication( reportHeading, reportHeading + " Text " + essay );
				
				publishPublication( reportHeading, new String[]{ "SimpleCheck" } );
			}

			reportHeading = "Live Reports Set {B} - " + i;
			if( createNewPublication( authorNames, reportHeading, "" ) )
			{
				editPublication( reportHeading, reportHeading + " Text " + essay );

				publishPublication( reportHeading, new String[]{ "SimpleCheck" } );
			}


			String comment = commentOnPublication( reportHeading, authorNames );
			
			while( comment == null )
			{
				try
				{
					wait( 10 );
				}
				catch( InterruptedException e )
				{
					// TODO Auto-generated catch block
                    System.out.println( "Sleep 1 Exception\n" );
					e.printStackTrace();
				}
				
				comment = commentOnPublication( reportHeading, authorNames );
			}
			editPublication( comment, "Im sample a comment." );
			publishPublication( comment, new String[]{ "SimpleCheck" } );
			


			comment = commentOnPublication( reportHeading, authorNames );
			while( comment == null )
			{
				try
				{
					wait( 10 );
				}
				catch( InterruptedException e )
				{
					// TODO Auto-generated catch block
                    System.out.println( "Sleep 2 Exception\n" );
					e.printStackTrace();
				}
				
				comment = commentOnPublication( reportHeading, authorNames );
			}
			editPublication( comment, "Im another sample comment." );
			publishPublication( comment, new String[]{ "SimpleCheck" } );
		}
	}

	public void randomChange( )
	{
		String[] authorNames = { "Default" };
		

		String reportHeading = "Random Publication";
		if( createNewPublication( authorNames, reportHeading, "" ) )
		{
			editPublication( reportHeading, "LIVE VIEW - Text\n---------------------------" );
			
			publishPublication( reportHeading, new String[]{ "SimpleCheck" } );
		}

		reportHeading = "Random Publication 2";
		if( createNewPublication( authorNames, reportHeading, "" ) )
		{
			editPublication( reportHeading, "LIVE VIEW\n---------------------------" );
			
			publishPublication( reportHeading, new String[]{ "SimpleCheck" } );
			
			editPublication( commentOnPublication( reportHeading, authorNames ), "Im Someone" );
		}
	}
}
