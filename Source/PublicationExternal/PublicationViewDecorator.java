package PublicationExternal;

import java.util.ArrayList;

public class PublicationViewDecorator extends PublicationView
{
	private ArrayList< PublicationView > child;

	public PublicationViewDecorator( String publication )
	{
		super( publication );

		 child = new ArrayList< PublicationView >( 1 );
	}

	@ Override
	public int size( )
	{
		return child.size( );
	}

	@ Override
	public PublicationView getChild( int i )
	{
		return child.get( i );
	}

	@ Override
	public boolean add( PublicationView newChild )
	{
		if( child.size( ) == 0 )
		{
			child.add( newChild );
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public PublicationView remove( int index )
	{
		return child.remove( index );
	}
	
	public boolean remove( PublicationView child )
	{
		return child.remove( child );
	}
}
