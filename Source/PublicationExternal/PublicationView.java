package PublicationExternal;

abstract public class PublicationView
{
	private String publication;
	
	public PublicationView( String publication )
	{
		this.setPublication( publication );
	}

	private void setPublication( String publication )
	{
		this.publication = publication;
	}

	public String getPublication( )
	{
		return publication;
	}

	@ Override
	public boolean equals( Object obj )
	{
		PublicationView view = ( PublicationView )( obj );
		
		return view.getPublication( ).equals( publication );
	}

	abstract public int size( );

	abstract public PublicationView getChild( int i );
	
	abstract public boolean add( PublicationView child );
	
	abstract public PublicationView remove( int index );
	
	abstract public boolean remove( PublicationView child );
}
