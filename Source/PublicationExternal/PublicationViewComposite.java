package PublicationExternal;

import java.util.ArrayList;

/**
 * A connection between authors and reports linked to there respective comments; - LIVE! NODES!
 * 
 * @author Nicholas Welters
 *
 */

public class PublicationViewComposite extends PublicationView
{
	private ArrayList< PublicationView > children;
	
	public PublicationViewComposite( String publication )
	{
		super( publication );

		children = new ArrayList< PublicationView >( );
	}

	public int size( )
	{
		return children.size( );
	}

	public PublicationView getChild( int i )
	{
		return children.get( i );
	}
	
	public boolean add( PublicationView child )
	{
		return children.add( child );
	}
	
	public PublicationView remove( int index )
	{
		return children.remove( index );
	}
	
	public boolean remove( PublicationView child )
	{
		return children.remove( child );
	}
}
