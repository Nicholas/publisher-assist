package PublicationExternal;


public class PublicationViewLeaf extends PublicationView
{
	public PublicationViewLeaf( String publication )
	{
		super( publication );
	}

	@ Override
	public int size( )
	{
		return 0;
	}

	@ Override
	public PublicationView getChild( int i )
	{
		return null;
	}

	@ Override
	public boolean add( PublicationView child )
	{
		return false;
	}

	@ Override
	public PublicationView remove( int index )
	{
		return null;
	}

	@ Override
	public boolean remove( PublicationView child )
	{
		return false;
	}
}
