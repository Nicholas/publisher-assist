package Author;
/**
 * A basic author class to let authors have access to there information
 * 
 * 	Must know all the messages sent and then each message gets a comment that must notify the author.
 * 
 * @author Nicholas Welters
 *
 */

import java.util.LinkedList;

import PublicationExternal.PublicationView;
import PublicationExternal.PublicationViewLeaf;


public class Author
{
	//Some random information
	private String name;
	
	private LinkedList< PublicationView > notifications;
	private LinkedList< PublicationView > drafts;
	private LinkedList< PublicationView > pending;
	private LinkedList< PublicationView > live;
	
	public Author( String name )
	{
		this.name = name;
		

		notifications = new LinkedList< PublicationView >( );
		drafts = new LinkedList< PublicationView >( );
		pending = new LinkedList< PublicationView >( );
		live = new LinkedList< PublicationView >( );
	}
	
	public String getName( )
	{
		return name;
	}

	public boolean addDraft( String publication )
	{
		return drafts.add( new PublicationViewLeaf( publication ) );
	}
	
	public boolean setPending( String publication )
	{
		PublicationView view = new PublicationViewLeaf( publication );
		
		if( drafts.remove( view ) )
		{
			return pending.add( view );
		}
		
		return false;
	}
	
	public boolean setLive( String publication )
	{
		PublicationView view = new PublicationViewLeaf( publication );
		
		if( pending.remove( view ) )
		{
			return live.add( view );
		}
		
		return false;
	}

	public void addNotification( PublicationView publication )
	{
		notifications.add( publication );
	}

	public LinkedList< PublicationView > getNotifications( )
	{
		return notifications;
	}

	public LinkedList< PublicationView > getDrafts( )
	{
		return drafts;
	}

	public LinkedList< PublicationView > getPending( )
	{
		return pending;
	}

	public LinkedList< PublicationView > getLive( )
	{
		return live;
	}

	public boolean remove( String publication )
	{
		return drafts.remove( new PublicationViewLeaf( publication ) );
	}

	public boolean removeNotification( String publication )
	{
		// TODO Auto-generated method stub
		return notifications.remove( new PublicationViewLeaf( publication ) );
		
	}

	public boolean setDraft( String publication )
	{
		PublicationView view = new PublicationViewLeaf( publication );
		
		if( pending.remove( view ) )
		{
			return drafts.add( view );
		}
		
		return false;
	}
}
