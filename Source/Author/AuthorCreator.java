package Author;

import Tools.CreatorStratergy;

public class AuthorCreator implements CreatorStratergy< Author, AuthorKey >
{
	@ Override
	public Author FactoryMethod( AuthorKey key )
	{
		return new Author( key.getAuthor( ) );
	}
}
