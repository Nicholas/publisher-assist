package Author;

import Tools.FlyweightKey;

public class AuthorKey extends FlyweightKey< Author >
{
	private String author;

	public AuthorKey( String key )
	{
		super( key );

		setAuthor( key );
	}

	private void setAuthor( String author )
	{
		this.author = author;
	}

	public String getAuthor( )
	{
		return author;
	}

	
}
