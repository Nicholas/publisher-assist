package Report;

/**
 * A simple report type class that may have images and such but all i care for is text right now.
 * 
 * @author Nicholas Welters
 *
 */
public class Report
{
	private String heading;
	private String text;
	
	private Report( String heading, String text )
	{
		this.heading = heading;
		this.text = text;
	}
	
	public Report( String heading )
	{
		this( heading , "" );
	}

	public String getHeading( )
	{
		return heading;
	}
	
	public String getText( )
	{
		return text;
	}
	
	public void setText( String text )
	{
		this.text = text;
	}
}
