package Report;

import Tools.FlyweightKey;

public class ReportKey extends FlyweightKey< Report >
{
	private String heading;
	
	public ReportKey( String key )
	{
		super( key );
		
		setHeading( key );
	}

	private void setHeading( String heading )
	{
		this.heading = heading;
	}

	public String getHeading( )
	{
		return heading;
	}
}
