package Report;

import Tools.CreatorStratergy;

public class ReportCreator implements CreatorStratergy< Report, ReportKey >
{
	@ Override
	public Report FactoryMethod( ReportKey key )
	{
		return new Report( key.getHeading( ) );
	}
}
