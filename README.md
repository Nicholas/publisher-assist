# Publication Assistant

The goal of this application is to demonstrate a framwork for a system that can be used to assist writers and publishers to approve new documents.

Documents are created by the writers who then submit their documents for automated and/or manual checks before being approved and published.

This java implementation uses a client server based architecture to maintain a central control for processing documents.
The system is able to dynamically select which checks will be used on a document as well as being able to load the required classes to perform the checks at run time.
This allows it to be easily extended with new features with out having to modify the core application.